package org.npci.puzzles;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.jupiter.api.Nested;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class ThermometerTest {
    @Nested
    @DisplayName("Validate Fahrenheit.")
    class validateFahrenheit{
        @ParameterizedTest
        @ValueSource(doubles = {-500, -459.68})
        void shouldReturnIllegalArgumentExceptionOnInvalidTemperature(Double temperature){
            assertThrows(IllegalArgumentException.class,() -> new Fahrenheit(temperature));
        }

        @Test
        void shouldEquateTwoFahrenheitObjectsWithSameValue(){
            assertThat(new Fahrenheit(100.0), equalTo(new Fahrenheit(100.0)));
        }

        @Test
        void shouldNotEquateTwoFahrenheitObjectsWithDifferentValue() {
            assertThat(new Fahrenheit(100.0), not(new Fahrenheit(101.0)));
        }

        @Test
        void shouldNotEquateTwoFahrenheitObjectsWithNullValue() {
            assertThat(new Fahrenheit(100.0), not(equalTo(null)));
        }

        @Test
        void shouldNotEquateFahrenheitObjectsWithDifferentObject() {
            assertThat(new Fahrenheit(100.0), not(equalTo("Something")));
        }

        @Test
        void shouldReturnFalseUponAddingTwoFahrenheitObjectsWithSameValueToHashSet(){
            Fahrenheit f1 = new Fahrenheit(100.0);
            Fahrenheit f2 = new Fahrenheit(100.0);
            HashSet<Fahrenheit> hashSet = new HashSet<>();
            hashSet.add(f1);
            boolean actual = hashSet.add(f2);
            assertThat(false, equalTo(actual));
        }

        @Test
        void shouldReturnTrueUponAddingTwoFahrenheitObjectsWithDifferentValueToHashSet(){
            Fahrenheit f1 = new Fahrenheit(100.0);
            Fahrenheit f2 = new Fahrenheit(101.0);
            HashSet<Fahrenheit> hashSet = new HashSet<>();
            hashSet.add(f1);
            boolean actual = hashSet.add(f2);
            assertThat(true, is(actual));
        }
    }

    @Nested
    @DisplayName("Validate Rupee.")
    class validateRupee{
        @ParameterizedTest
        @ValueSource(doubles = {-1.0, -2.0})
        void shouldReturnIllegalArgumentExceptionOnInvalidPrice(Double price){
            assertThrows(IllegalArgumentException.class,() -> new Rupee(price));
        }

        @Test
        void shouldEquateTwoRupeeObjectsWithSameValue(){
            assertThat(new Rupee(100.0), equalTo(new Rupee(100.0)));
        }
    }

    @Nested
    @DisplayName("Validate Thermometer.")
    class validateThermometer{
        @Test
        void shouldReturnPriceOfThermometer(){
            assertThat(new Rupee(100.0), equalTo(new Thermometer(new Rupee(100.0)).getPrice()));
        }

        @Test
        void shouldReturnTemperatureOfThermometer(){
            Thermometer thermometer = new Thermometer(new Rupee(100.0));
            thermometer.setTemperature(new Fahrenheit(100.0));
            assertThat(new Fahrenheit(100.0), equalTo(thermometer.getTemperature()));
        }
    }
}
