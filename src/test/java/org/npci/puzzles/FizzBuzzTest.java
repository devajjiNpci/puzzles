package org.npci.puzzles;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class FizzBuzzTest {
    FizzBuzz fizzBuzz;

    @BeforeEach
    void setUp() {
        fizzBuzz = new FizzBuzz();
    }

    @ParameterizedTest
    @ValueSource(ints = {3, 6, 9, 12, 18})
    void shouldReturnFizzIfInputIsMultipleOf3(int number) {
        String expected = "Fizz";
        String actual = fizzBuzz.convert(number);
        assertEquals(expected, actual);
    }

    @Test
    void shouldReturnBuzzIfInputIsMultipleOf5() {
        String expected = "Buzz";

        String actual = fizzBuzz.convert(5);
        assertEquals(expected, actual);
    }

    @Test
    void shouldReturnFizzBuzzWhenInputIsMultipleOf3And5() {
        String expected="FizzBuzz";
        String actual = fizzBuzz.convert(15);
        assertEquals(expected,actual);
    }

    @Test
    void shouldReturnSameInputIfNumberIsNotMultipleOf3And5() {
        String expected="14";
        String actual = fizzBuzz.convert(14);
        assertEquals(expected,actual);
    }


    @ParameterizedTest
    @ValueSource(ints = {0 , -1, -2})
    void shouldThrowExceptionIfInputIsLessThan1(int input) {
        assertThrows(IllegalArgumentException.class,()-> fizzBuzz.convert(input));
    }
}