package org.npci.puzzles;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class NumberCruncherTest {

    @Test
    void shouldReturnMinimumValueOfACollection() {
        Integer expected = 1;
        List<Integer> values = Arrays.asList(1, 2, 3);
        NumberCruncher numberCruncher = new NumberCruncher(values);

        Integer actual =  numberCruncher.min();
        assertEquals(expected, actual);
    }

    @Test
    void minShouldThrowIllegalArgumentExceptionForAnEmptyCollection() {
        NumberCruncher numberCruncher = new NumberCruncher(Collections.EMPTY_LIST);
        assertThrows(IllegalArgumentException.class, numberCruncher::min);
    }

    @Test
    void shouldReturnAvgValueOfACollection() {
        Double expected = 2.0;
        List<Integer> values = Arrays.asList(1, 2, 3);
        NumberCruncher numberCruncher = new NumberCruncher(values);

        Double actual = numberCruncher.avg();
        assertEquals(expected, actual);
    }




}

