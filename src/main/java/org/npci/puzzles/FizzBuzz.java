package org.npci.puzzles;

public class FizzBuzz {

    public String convert(int input) {
        if(input < 1)
            throw new IllegalArgumentException();

        boolean isMultipleOf3 = input % 3 == 0;
        boolean isMultipleOf5 = input % 5 == 0;

        StringBuilder result = new StringBuilder();

        if(isMultipleOf3)
            result.append("Fizz");
        if(isMultipleOf5)
            result.append("Buzz");
        if(!isMultipleOf3 && !isMultipleOf5)
            result.append(input);

        return result.toString();
    }
}
