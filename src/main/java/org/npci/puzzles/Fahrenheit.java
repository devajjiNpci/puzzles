package org.npci.puzzles;

import java.util.Objects;

public class Fahrenheit {
    private final Double temperature;

    public Fahrenheit(Double temperature){
        if(temperature < -459.67)
            throw new IllegalArgumentException();
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return temperature + " F";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fahrenheit that = (Fahrenheit) o;
        return temperature.equals(that.temperature);
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperature);
    }
}
