package org.npci.puzzles;

import java.util.List;

public class NumberCruncher {

    private List<Integer> values;

    public NumberCruncher(List<Integer> values) {
        this.values = values;
    }

    public Integer min() {
        return values.stream().min(Integer::compareTo)
                .orElseThrow(IllegalArgumentException::new);
    }

    public Double avg() {
        return values.stream().mapToDouble(Integer::doubleValue).average().orElse(0);
    }
}
