package org.npci.puzzles;

public class Thermometer {
    private final Rupee price;
    private Fahrenheit temperature;

    public Thermometer(Rupee price){
        this.price = price;
    }

    public Rupee getPrice() {
        return this.price;
    }

    public Fahrenheit getTemperature(){
        return this.temperature;
    }

    public void setTemperature(Fahrenheit temperature) {
        this.temperature = temperature;
    }
}
