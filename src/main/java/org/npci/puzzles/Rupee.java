package org.npci.puzzles;

import java.util.Objects;

public class Rupee {
    private final Double price;

    public Rupee(Double price){
        if(price < 0.0)
            throw new IllegalArgumentException();
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rupee rupee = (Rupee) o;
        return price.equals(rupee.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price);
    }

    @Override
    public String toString() {
        return price + " INR";
    }
}
