# puzzles

Bunch of puzzles for NPCI

## Puzzle 1: FizzBuzz Puzzle

Given a number, the program should: 
Return "Fizz" if the number is a multiple of 3; 
Return "Buzz" if the number is a multiple of 5; 
Return "FizzBuzz" if the number is a multiple of both.
Return the same number if the number is not a multiple of one of them or both; 


Project is built using Gradle 6 and JDK 11

## Steps to compile application

## Steps to run application 

